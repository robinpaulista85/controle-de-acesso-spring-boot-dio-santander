# Construindo um sistema de controle de ponto e acesso com Spring Boot.

Author: [Robinson Dias]

Api rest para gerenciamento de ponto e controle de acesso.

Utilizamos nesse projeto a seguinte linguagem e frameworks:

Java | Spring boot | Spring Data Jpa | Hibernate | Lombok | swagger


![image](https://user-images.githubusercontent.com/6122791/123674340-e3a4ae00-d817-11eb-8e50-4b95c0d0efc2.png)

Para maiores informações e acesso ao curso, queira acessar a plataforma da Digital Innovation One com o seguinte endereço:
https://web.digitalinnovation.one/lab/construindo-um-sistema-de-controle-de-ponto-e-acesso-com-spring-boot/learning/9b94d9ed-c224-4a7e-8ffb-2e063fcd8a74

